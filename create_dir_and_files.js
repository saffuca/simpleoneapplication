const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
fs.readFile(
    path.join(__dirname, 'map.json'),
    'utf-8',
    (err, data) => {
        if (err) {
            throw err
        } else {
            data = JSON.parse(data)
                // console.log(data)
            data.forEach(application => {
                fs.mkdir(
                        path.join(__dirname, 'applications', application.name),
                        err => {
                            if (err) {

                            } else {
                                console.log(chalk.green(`Папка ${application.name} создана`))
                            }
                        }
                    )
                    // console.log(application)
                for (let essence in application['essences']) {

                    fs.mkdir(
                        path.join(__dirname, 'applications', application.name, essence),
                        err => {
                            if (err) {
                                // throw err
                            } else {
                                console.log(chalk.green(`Папка ${essence} создана`))
                            }
                        }
                    )

                    application['essences'][essence].forEach(record => {
                        fs.writeFile(
                            path.join(__dirname, 'applications', application.name, essence, `${record.sys_id}.json`),
                            record.json_copy,
                            err => {
                                if (err) {
                                    // throw err
                                } else {
                                    console.log(chalk.green(`Файл ${record.sys_id} создан`))
                                }
                            }
                        )
                    })
                }
            })
        }
    }
)
(() => {
    const vcsStorage = () => {
        return new Promise((resolve, reject) => {
            let storage = {}
            const vcs = new SimpleRecord('sys_vcs_record')
            vcs.addQuery('is_current', true);
            vcs.query(() => {
                while (vcs.next()) {
                    if (!storage[vcs.table_name]) {
                        storage[vcs.table_name] = []
                    }

                    storage[vcs.table_name].push({
                        'sys_id': vcs.record_id,
                        'json_copy': vcs.json_copy
                    })
                }
                resolve(storage)
            })
        })
    }
    const getListOfApplications = () => {
        return new Promise((resolve, reject) => {
            const record = new SimpleRecord('sys_application');
            record.query(() => {
                const applications = [];
                while (record.next()) {
                    applications.push({
                        'name': record.name,
                        'sys_id': record.sys_id,
                        'essences': {}
                    });
                }
                resolve(applications)
            })
        })
    }

    const createStructureStorage = (applications) => {
        const promises = [];
        let storage = {};
        applications.forEach((application, index) => {

            if (!storage['application']) {
                storage['application'] = {};
            }
            storage['application'][application.name] = {
                'sys_id': application.sys_id,
                'essences': {}
            }
            promises.push(new Promise((resolve, reject) => {
                const tables = new SimpleRecord('sys_db_table');

                tables.addQuery('application_id', application.sys_id);
                tables.orderBy('is_vcs_enabled');
                tables.query(() => {

                    while (tables.next()) {
                        storage['application'][application.name]['essences'][tables.name] = {
                            'sys_id': tables.sys_id,
                            'title': tables.title,
                            'name': tables.name,
                            'is_vcs_enabled': Boolean(tables.is_vcs_enabled),
                            'records': []
                        }
                        resolve(storage)
                    }
                });
            }));
        })
        return Promise.all(promises)
    }

    const fillingStorage = async(storage) => {

        const tableList = []
        const promises = []
        const tables = new SimpleRecord('sys_db_table');
        tables.addQuery('is_vcs_enabled', true)
        tables.query(async() => {
            while (tables.next()) {
                promises.push(new Promise((resolve, reject) => {
                    tableList.push({
                        'name': tables.name,
                        'sys_id': tables.sys_id
                    })
                    resolve()
                }))
            }

            await Promise.all(promises)

            const recordPromises = []
            let storageVcs = await vcsStorage()
                // console.log(storageVcs)
            const recordWithOutApplication = []
            const recordWithTrashApplication = []
            const recordWithOutVcs = []
            tableList.forEach(table => {
                recordPromises.push(new Promise((resolve, reject) => {
                    const record = new SimpleRecord(table.name)
                    record.query(async() => {
                        while (record.next()) {
                            //запись без приложения
                            if (!record.application_id) {
                                recordWithOutApplication.push({
                                    'tableName': table.name,
                                    'sys_id': record.sys_id
                                })
                                reject()
                            }
                            const findApplication = storage.find(application => application.sys_id == record.application_id)
                            const findIndex = storage.findIndex(application => application.sys_id == record.application_id)
                                //приложения нет в системе
                            if (!findApplication) {
                                recordWithTrashApplication.push({
                                    'tableName': table.name,
                                    'sys_id': record.sys_id
                                })
                                reject()
                            } else {
                                //у приложения нет ключа
                                if (!Boolean(storage[findIndex]['essences'][table.name])) {
                                    storage[findIndex]['essences'][table.name] = []
                                }

                                //у приложения есть ключ
                                if (!!storageVcs[table.name]) {
                                    // console.log(storageVcs[table.name], record.sys_id, table.name)
                                    const findVcs = storageVcs[table.name].find(records => {
                                            return records.sys_id == record.sys_id
                                        })
                                        // console.log(findVcs)
                                    if (!!findVcs) {
                                        // console.log({
                                        //     'sys_id': record.sys_id,
                                        //     'json_copy': findVcs.json_copy
                                        // })
                                        storage[findIndex]['essences'][table.name].push({
                                            'sys_id': record.sys_id,
                                            'json_copy': findVcs.json_copy
                                        })
                                    } else {
                                        recordWithOutVcs.push({
                                            'tableName': table.name,
                                            'sys_id': record.sys_id
                                        })
                                    }
                                } else {
                                    console.log(record.getTableName(), record.sys_id, 'Без vcs')
                                }
                            }
                        }
                        resolve()
                    })
                }))
            })
            await Promise.allSettled(recordPromises)
                .then(() => {
                    console.log('here')
                    console.log(JSON.stringify(storage))
                })
                .catch(() => console.log('failed'))
            console.log(recordWithOutApplication)
            console.log(recordWithTrashApplication)
            console.log(recordWithOutVcs)
        })
    }
    getListOfApplications()
        .then(result => fillingStorage(result))
})()